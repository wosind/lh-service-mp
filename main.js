import Vue from "vue";
import App from "./App";
import global_ from "./static/js/global";
// import map_ from "./static/js/qqmap-wx-jssdk.min.js";
import store from "./store";
// Vue.prototype.MAP = map_;
Vue.prototype.$store = store;
Vue.config.productionTip = false;
Vue.prototype.GLOBAL = global_;
App.mpType = "app";

const app = new Vue({
  store,
  ...App,
});

import _plugin from "./utils/xxplugins";
Vue.use(_plugin, app);

app.$mount();
