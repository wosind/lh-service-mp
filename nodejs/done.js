/*
构造省份列表
运行：node nodejs/done.js
by xiaox 2021-04-07
*/

const fs = require('fs');
const path = require('path');

// import airport from '@/common/airport.js'
const province  = require("./province.js");

// 遍历文件
function walkSync(targetPath, callback) {
	fs.readdirSync(targetPath).forEach(function(fileName) {
		var filePath = path.join(targetPath, fileName);
		var stat = fs.statSync(filePath);
		if (stat.isFile()) {
			callback(filePath);
		} else if (stat.isDirectory()) {
			walkSync(filePath, callback);
		}
	});
}

// 写入json文件
function writeFile(filename) {
	console.log(`开始写入文件：${filename}`);
	fs.writeFile(filename, content, function(err) {
		if (err) {
			return console.error(err);
		}
	});
	console.log("写入结束");
}

// 获取页面配置信息
function getPageSet(filename) {	
	var res = {};
	let data = fs.readFileSync(filename);
	let content = data.toString();	
		
	// 抓取 NAVTITLE:..., 配置项 ======================================
	let title = content.match(/NAVTITLE[^,]+/g);
	title = title ? title[0].split(":")[1].replace(/\"| /g,"") : "";	
	res["style"] = {
		"navigationBarTitleText":title
	};
	
	return res;
}

//=============================================================================
console.log(`${'='.repeat(60)}\nstart done.js\n${'='.repeat(60)}`)
var lpath = path.resolve(__dirname, `../pages_sub`);

console.log(fs.readdirSync(lpath));


// console.log(JSON.stringify(res));
// let content = JSON.stringify(res);
// writeFile("pages.json");
