import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

let cinfo = uni.getStorageSync("updatecustomerInfo");
let oid = uni.getStorageSync("openid");
let isNotice = uni.getStorageSync("isNotice");
const store = new Vuex.Store({
  state: {
    isNotice: isNotice || "N",
    customerInfo: cinfo ? cinfo : {},
    userInfo: {},
    hasInfo: cinfo ? true : false,
    openid: oid || "",
    currentAsk: {
      // Ask: "11"
      // IsBind: "N"
      // Phone: "33"
      // RemoteAccount: "44"
      // RemoteAuth: "55"
      // Sender: "22"
      // openid: "oxfnA4rmvKzIUz1jlWFIeYbMssUE"
      // session_id: ""
      Ask: "",
      Create_id: "",
      IsDependRemote: "",
      IsWinAuth: "",
      Phone: "",
      RemoteAccount: "",
      RemoteAuth: "",
      RemoteType: "",
      Sender: "",
      WinPwd: "",
      WinUser: "",
      Customer_id: "",
      Session_id: "",
    },
  },
  mutations: {
    updateIsNotice(state, val) {
      state.isNotice = val;
      uni.setStorageSync("isNotice", val);
    },

    updateCustomerInfo(state, infos) {
      state.customerInfo = infos;
      state.hasInfo = true;
      uni.setStorageSync("updatecustomerInfo", infos);
    },
    updateAsk(state, infos) {
      state.currentAsk = infos;
      uni.setStorageSync("currentAsk", infos);
    },
    updateOpenid(state, id) {
      state.openid = id;
      uni.setStorageSync("openid", id);
    },

    authorize(state, provider) {
      console.log("authorize:", provider);
      state.hasRole = true;
      state.userInfo.role = provider.utype;
      state.userInfo.sid = provider.sid;
      state.userInfo.name = provider.name;
      uni.setStorageSync("userInfo", provider);
    },
    cancelAuthorize(state) {
      state.hasRole = false;
      state.userInfo = {};
      uni.removeStorage({
        key: "userInfo",
      });
    },
  },
});

export default store;
