/**
 * @description 获取localStorage数据
 * @key {string} localStorage保存的键名
 * @returns {string} 返回字符串数据
 */
const getItem = function (key) {
  try {
    const value = uni.getStorageSync(key)
    return value
  } catch (e) {
    return ''
  }
}

/**
 * @description 写入localStorage数据
 * @key {string} localStorage保存的键名
 * @data {string} 需要写入localStorage的字符串
 * @returns {Blob}} 返回执行状态Blob
 */
const setItem = function (key, data) {
  try {
    uni.setStorageSync(key, data)
    true
  } catch (e) {
    return false
  }
}

const confirm = function (msg) {}

export default {
  getItem,
  setItem,
}
